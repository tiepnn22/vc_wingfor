<?php get_header(); ?>
<?php
	use NF\View\Facades\View;
?>
<?php
	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$term_id = $term->term_id;
	$term_name = $term->name;
	$taxonomy_name = 'product-category';
?>
<section class="product-list">
	<div class="container">
		<div class="row">

			<?php
				echo View::render('partials.sidebar');
			?>

			<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 product">

				<?php
					echo View::render('partials.slide-all-page');
				?>

				<div class="main-title">
					<a>
						<h2><?php echo $term_name;?></h2>
					</a>
				</div>

				<div class="product-content">
					<div class="row">
						<?php
							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
							$query = wingfor_custom_posttype_query_paged('products', 'product-category', $term_id, $paged, 6);
						?>
						<?php
							if($query->have_posts()) {
								while ($query->have_posts()) {
									$query->the_post();

									$data = [
										'id' => get_the_ID(),
										'url' => get_the_permalink(),
										'img' => wingfor_get_thumbnail_url('product'),
										'title' => get_the_title(),
										'content' => get_the_content(),
										'date' => get_the_date('Y/m/d'),
										'excerpt' => cut_string(get_the_excerpt(),400,'...'),
									];

									echo View::render('partials.taxonomy', $data);

								}
								wp_reset_postdata();
							}
						?>
					</div>
				</div>

				<nav class="navigation">
					<?php wp_pagenavi(array( 'query' => $query )); ?>
				</nav>

				<div class="page-info description-category">
					<?php echo wpautop(types_render_termmeta( "mo-ta-danh-muc", array('term_id' => $term_id , 'output'=>'raw') )); ?>
				</div>

			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>