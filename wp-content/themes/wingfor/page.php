<?php get_header(); ?>
<?php
	use NF\View\Facades\View;
?>
<section class="page">
	<div class="container">
		<div class="row">

			<?php
				echo View::render('partials.sidebar');
			?>

			<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 page-content">

				<?php
					echo View::render('partials.slide-all-page');
				?>

				<?php
					if(have_posts()) {
						while (have_posts()) {
							the_post();

							$data = [
								'id' => get_the_ID(),
								'url' => get_the_permalink(),
								'img' => wingfor_get_thumbnail_url('product'),
								'title' => get_the_title(),
								'content' => get_the_content(),
								'date' => get_the_date('Y/m/d'),
								'excerpt' => cut_string(get_the_excerpt(),400,'...'),
							];

							echo View::render('partials.page', $data);

						}
						wp_reset_query();
					}
				?>

			</div>

		</div>
	</div>
</section>
<?php get_footer(); ?>