<?php
/**
 * Enqueue scripts and stylesheet
 */
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
add_action('wp_enqueue_scripts', 'theme_enqueue_style');

function theme_enqueue_style()
{
    // wp_enqueue_style(
    //     'template-vendor',
    //     get_stylesheet_directory_uri() . '/dist/styles/vendor.css',
    //     false
    // );
    wp_enqueue_style(
        'template-style',
        get_stylesheet_directory_uri() . '/dist/styles/main.css',
        false
    );
}

function theme_enqueue_scripts()
{
    // wp_enqueue_script(
    //     'template-packages',
    //     get_stylesheet_directory_uri() . '/dist/scripts/vendor.js',
    //     'jquery',
    //     '1.0',
    //     true
    // );
    wp_enqueue_script(
        'template-scripts',
        get_stylesheet_directory_uri() . '/dist/scripts/main.js',
        'jquery',
        '1.0',
        true
    );
}

//lay duong dan
if (!function_exists('asset')) {
    function asset($path)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/dist/' . $path);
    }
}

//shortcode có thể chạy được trong widget
add_filter('widget_text','do_shortcode');

//đăng ký menu và img
if (!function_exists('wingforSetup')) {
    function wingforSetup()
    {
        // Register menus
        register_nav_menus( array(
            'primary' => __( 'Menu primary', 'wingfor' ),
        ) );

        // add_theme_support('menus');
        add_theme_support('post-thumbnails');
        add_image_size('product', 235, 165, true);
    }

    add_action('after_setup_theme', 'wingforSetup');
}


//dang ky sidebar
if (!function_exists('wingforWidgets')) {
    function wingforWidgets()
    {
        $sidebars = [
            [
                'name'          => __('Sidebar', 'wingfor'),
                'id'            => 'sidebar',
                'description'   => __('This is sidebar', 'wingfor'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>',
            ],

            [
                'name'          => __('Header Contact', 'wingfor'),
                'id'            => 'header-contact',
                'description'   => __('This is contact for header', 'wingfor'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>',
            ],
            [
                'name'          => __('Header Slogan', 'wingfor'),
                'id'            => 'header-slogan',
                'description'   => __('This is slogan for header', 'wingfor'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>',
            ],
            [
                'name'          => __('Header Slogan Center', 'wingfor'),
                'id'            => 'header-slogan-center',
                'description'   => __('This is slogan center for header', 'wingfor'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>',
            ],
            [
                'name'          => __('Header Socical', 'wingfor'),
                'id'            => 'header-socical',
                'description'   => __('This is socical for header', 'wingfor'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>',
            ],


            [
                'name'          => __('Home Slide', 'wingfor'),
                'id'            => 'home-slide',
                'description'   => __('This is sidebar for home slide', 'wingfor'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>',
            ],
            [
                'name'          => __('Slide All Page', 'wingfor'),
                'id'            => 'slide-all-page',
                'description'   => __('This is sidebar for slide all page', 'wingfor'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>',
            ],
            [
                'name'          => __('Slide All Page Text', 'wingfor'),
                'id'            => 'slide-all-page-text',
                'description'   => __('This is sidebar for slide all page text', 'wingfor'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>',
            ],


            [
                'name'          => __('Footer-Contact', 'wingfor'),
                'id'            => 'footer_contact',
                'description'   => __('This is contact for footer', 'wingfor'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>',
            ],
            [
                'name'          => __('Footer-Design', 'wingfor'),
                'id'            => 'footer_design',
                'description'   => __('This is design for footer', 'wingfor'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h3 class="widget-title">',
                'after_title'   => '</h3>',
            ],
        ];

        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }
    add_action('widgets_init', 'wingforWidgets');
}
