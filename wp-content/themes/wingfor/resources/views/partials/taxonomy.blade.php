<article class="col-lg-4 col-md-4 col-sm-4 col-xs-4 item">
	<figure>
		<a href="{{ $url }}">
			<img src="{{ $img }}" alt="{{ $title }}" />
		</a>
	</figure>
	<div class="info">
		<div class="title">
			<a href="{{ $url }}">
				<h3>{{ $title }}</h3>
			</a>
		</div>
	</div>
</article>