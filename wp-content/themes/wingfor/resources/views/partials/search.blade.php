<article class="item">
	<figure>
		<a href="{{ $url }}">
			<img src="{{ $img }}" alt="{{ $title }}" />
		</a>
	</figure>
	<div class="info">
		<div class="title">
			<a href="{{ $url }}">
				<h3>{{ $title }}</h3>
			</a>
		</div>
		<div class="date">
			<i class="fa fa-clock-o" aria-hidden="true"></i>
			<span>{{ $date }}</span>
		</div>
		<div class="desc">
			{{ $excerpt }}
		</div>
	</div>
</article>