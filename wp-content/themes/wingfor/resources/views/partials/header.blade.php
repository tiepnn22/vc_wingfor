<header class="header">
	<div class="header-top">
		<div class="container">
			<div class="header-top-content">
				<div class="socical">
					@php(dynamic_sidebar('header-socical'))
				</div>
				<div class="language">
					{!! do_action('wpml_add_language_selector') !!}
				</div>
			</div>
		</div>
	</div>

	<div class="header-content">
		<div class="container">
			<div class="logo">
				<?php
                    if ( function_exists( 'the_custom_logo' ) ) {
					    the_custom_logo();
					}
	            ?>
	            <div class="slogan">
					@php(dynamic_sidebar('header-slogan'))
	            </div>
			</div>
			<div class="header-content-right">
		        <div class="header-slogan-center">
					@php(dynamic_sidebar('header-slogan-center'))
		        </div>
				<div class="contact-header">
					@php(dynamic_sidebar('header-contact'))
				</div>
			</div>
		</div>
	</div>
	<nav class="menu">
		<div class="container">
			<div class="main-menu">
				<?php
	                if(function_exists('wp_nav_menu')){
	                    $args = array(
	                        'theme_location' => 'primary',
	                        'link_before'=>'',
	                        'link_after'=>'',
	                        'container_class'=>'',
	                        'menu_class'=>'menu-primary',
	                        'menu_id'=>'',
	                        'container'=>'ul',
	                        'before'=>'',
	                        'after'=>''
	                    );
	                    wp_nav_menu( $args );
	                }
	            ?>
			</div>
			<div class="mobile-menu"></div>
		</div>
	</nav>
</header>
