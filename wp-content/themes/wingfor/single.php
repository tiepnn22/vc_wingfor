<?php get_header(); ?>
<?php
	use NF\View\Facades\View;
?>
<?php
	$a = wp_get_post_categories(get_the_ID());
	$id_category = $a[0];
	$name_category = get_cat_name( $id_category );
?>
<section class="single-post">
	<div class="container">
		<div class="row">

			<?php
				echo View::render('partials.sidebar');
			?>

			<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 page-content">

				<?php
					echo View::render('partials.slide-all-page');
				?>

				<div class="main-title">
					<a>
						<h2>
							<?php echo $name_category;?>
						</h2>
					</a>
				</div>

				<?php
					if(have_posts()) {
						while (have_posts()) {
							the_post();

							$data = [
								'id' => get_the_ID(),
								'url' => get_the_permalink(),
								'img' => wingfor_get_thumbnail_url('product'),
								'title' => get_the_title(),
								'content' => get_the_content(),
								'date' => get_the_date('Y/m/d'),
								'excerpt' => cut_string(get_the_excerpt(),400,'...'),
							];

							echo View::render('partials.single', $data);

						}
						wp_reset_query();
					}
				?>

				<?php
					echo View::render('partials.comment-like-share');
					echo View::render('partials.related-post');
				?>

			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>