<?php
//widget show post
class show_post_category extends WP_Widget {
    function __construct() {
        parent::__construct(
            'show_post_category',
            __('Show post category', 'wingfor') ,
            array( 'description'  =>  __('Show post category', 'wingfor') )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => __('Show post category', 'wingfor'),
            'id_category' => 2,
            'post_number' => 10,
        );
        $instance = wp_parse_args( (array) $instance, $default );

        $title = esc_attr($instance['title']);
        $id_category = esc_attr($instance['id_category']);
        $post_number = esc_attr($instance['post_number']);

        echo '<p>'; _e('Title: ', 'wingfor'); echo '<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        _e('Select category: ', 'wingfor'); echo '<select name="'.$this->get_field_name('id_category').'">';
            $categories = get_categories(  );
            foreach ( $categories as $category ) {
                if($id_category == $category->term_id) {
                     echo '<option name="'.$this->get_field_name('$category->term_id').'"  selected value="'.$category->term_id.'">'.$category->name.'</option>';
                }else{
                     echo '<option name="'.$this->get_field_name('$category->term_id').'"  value="'.$category->term_id.'">'.$category->name.'</option>';
                }
            }
        echo '</select>';
        echo '<p>'; _e('Number show post:', 'wingfor'); echo '<input type="number" class="widefat" name="'.$this->get_field_name('post_number').'" value="'.$post_number.'" /></p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['id_category'] = strip_tags($new_instance['id_category']);
        $instance['post_number'] = strip_tags($new_instance['post_number']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $id_category = $instance['id_category'];
        $post_number = $instance['post_number'];
        $link = get_term($id_category);

        echo $before_widget;
        echo '<div class="widget widget-show-post">';
        echo $before_title.$title.$after_title;
        echo '<div class="widget-content">';
            $query = new WP_Query(array('cat'=>$id_category,'order' => 'DESC','orderby' => 'date','showposts'=>$post_number));

            if ($query->have_posts()) {
                while ($query->have_posts() ) {
                    $query->the_post();
                    ?>
                    <article class="item">
                        <div class="title-sidebar">
                            <a href="<?php the_permalink();?>">
                                <h4><?php the_title();?></h4>
                            </a>
                        </div>
                    </article>
                    <?php
                }
            }

        echo '</div>';
        echo '</div>';
        echo $after_widget;
    }
}
function create_showpostcategory_widget() {
    register_widget('show_post_category');
}
add_action( 'widgets_init', 'create_showpostcategory_widget' );
