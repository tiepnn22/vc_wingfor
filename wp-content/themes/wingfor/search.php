<?php get_header(); ?>
<?php
	use NF\View\Facades\View;
?>
<section class="news-list">
	<div class="container">
		<div class="row">

			<?php
				echo View::render('partials.sidebar');
			?>

			<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 news">

				<?php
					echo View::render('partials.slide-all-page');
				?>

				<div class="main-title">
					<a>
						<h2><?php _e( 'Search', 'wingfor' ); ?></h2>
					</a>
				</div>

				<div class="news-content">
					<?php
						if(have_posts()) {
							while (have_posts()) {
								the_post();

								$data = [
									'id' => get_the_ID(),
									'url' => get_the_permalink(),
									'img' => wingfor_get_thumbnail_url('product'),
									'title' => get_the_title(),
									'content' => get_the_content(),
									'date' => get_the_date('Y/m/d'),
									'excerpt' => cut_string(get_the_excerpt(),400,'...'),
								];

								echo View::render('partials.search', $data);

							}
							wp_reset_query();
						}
					?>
				</div>

				<nav class="navigation">
					<?php wp_pagenavi(); ?>
				</nav>

			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>