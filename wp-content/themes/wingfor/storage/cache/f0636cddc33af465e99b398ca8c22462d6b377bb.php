<div class="main-title">
	<a>
		<h1><?php echo e($title); ?></h1>
	</a>
</div>

<div class="page-info">
	<?php the_content(); ?>
</div>