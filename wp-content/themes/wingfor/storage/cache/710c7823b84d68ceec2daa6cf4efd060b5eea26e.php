<article class="item">
	<figure>
		<a href="<?php echo e($url); ?>">
			<img src="<?php echo e($img); ?>" alt="<?php echo e($title); ?>" />
		</a>
	</figure>
	<div class="info">
		<div class="title">
			<a href="<?php echo e($url); ?>">
				<h3><?php echo e($title); ?></h3>
			</a>
		</div>
		<div class="date">
			<i class="fa fa-clock-o" aria-hidden="true"></i>
			<span><?php echo e($date); ?></span>
		</div>
		<div class="desc">
			<?php echo e($excerpt); ?>

		</div>
	</div>
</article>