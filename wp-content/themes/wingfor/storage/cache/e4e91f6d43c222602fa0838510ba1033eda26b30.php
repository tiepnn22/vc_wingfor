<footer>
	<div class="footer-top">
		<div class="container">
			<?php (dynamic_sidebar('footer_contact')); ?>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<?php (dynamic_sidebar('footer_design')); ?>
		</div>
	</div>
</footer>


<div class="popup-mobile">
	<div class='popup-content'>
		<div class="contact-header">
			<?php (dynamic_sidebar('header-contact')); ?>
		</div>
	</div>
	<div class='popup-plush'><i class='fa fa-plus' aria-hidden='true'></i></div>
</div>


<div id="back-to-top">
    <a href="javascript:void(0)"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
</div>


<?php
	use NF\View\Facades\View;
	echo View::render('partials.chat-facebook');
?>
