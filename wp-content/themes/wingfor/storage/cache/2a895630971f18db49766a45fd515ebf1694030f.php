<aside class="related related-post">
	<div class="main-title"><a><h2><?php _e('Related news', 'wingfor'); ?></h2></a></div>
	<div class="related-post-content">
			<?php
			    global $post;
			    $orig_post = $post;
			    $categories = get_the_category($post->ID);
			    if ($categories) {
				    $category_ids = array();
				    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
				    $args=array(
					    'category__in' => $category_ids,
					    'post__not_in' => array($post->ID),
					    'posts_per_page'=> 4,
					    'ignore_sticky_posts'=>1
				    );
				    $my_query = new wp_query( $args );
				    if( $my_query->have_posts() ) {
					    while( $my_query->have_posts() ) {
						    $my_query->the_post();?>

							    <article class="item">
									<div class="title">
										<a href="<?php the_permalink();?>" rel="bookmark">
											<h3><?php the_title();?></h3>
										</a>
									</div>
								</article>

						    <?php
					    }
				    }
			    }
			    $post = $orig_post;
			    wp_reset_query();
			?>
	</div>
</aside>


