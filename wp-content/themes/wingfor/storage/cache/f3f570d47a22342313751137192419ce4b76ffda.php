<article class="col-lg-4 col-md-4 col-sm-4 col-xs-4 item">
	<figure>
		<a href="<?php echo e($url); ?>">
			<img src="<?php echo wingfor_get_thumbnail_url('doctor'); ?>" alt="<?php the_title(); ?>" />
		</a>
	</figure>
	<div class="info">
		<div class="title">
			<a href="<?php echo e($url); ?>">
				<h3><?php echo e($title); ?></h3>
			</a>
		</div>
	</div>
</article>